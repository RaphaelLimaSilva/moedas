def valor_moedas(vlr):
    numeros = vlr
    flag = True
    if type(vlr) == str :
        numeros =""
        for x in vlr:
            try:
                int(x)
                numeros = numeros + x
            except:
                if x == "." and flag:
                    numeros = numeros + x
                    flag = False
                elif x == "," :
                    return "Você utilizou , no lugar de ."
    if numeros == "":
        return  "sem valor"

    numeros = float(numeros)
    numeros = numeros * 100
    numeros = int (numeros)
    result = ""

    if numeros == 0 :
        return "valor menor que 1 centavo"

    moedas = [100,50,25,10,5,1]
    for x in moedas :
        if numeros // x > 0 :
            qtd = numeros // x
            if x == 100:
                if qtd > 1:
                    result = result + f' {qtd} moedas de 1 real,'
                else:
                    result = result + f' {qtd} moeda de 1 real,'
            elif x == 1:
                if qtd > 1:
                    result = result + f' {qtd} moedas de 1 centavo,'
                else:
                    result = result + f' {qtd} moeda de 1 centavo,'
            else:
                if qtd > 1 :
                    result = result + f' {qtd} moedas de {x} centavos,'
                else:
                    result = result + f' {qtd} moeda de {x} centavos,'
            numeros = numeros - x * qtd

    result = result[:-1] + "."
    return result

