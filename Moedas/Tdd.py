import unittest
from Moeda.main import valor_moedas

class TestStringMethods(unittest.TestCase):
    def teste(self):
        self.assertNotEqual(valor_moedas("12,21"),valor_moedas(12.21))
    def teste(self):
        self.assertEqual(valor_moedas("12.21"),(12.21))
    def teste(self):
        self.assertEqual(valor_moedas(""),"sem valor" )
    def teste(self):
        self.assertEqual(valor_moedas(3.99)," 3 moedas de 1 real, 1 moeda de 50 centavos, 1 moeda de 25 centavos, 2 moedas de 10 centavos, 4 moedas de 1 centavo.")
    def teste(self):
        self.assertEqual(valor_moedas(0.009),"valor menor que 1 centavo")

if __name__ == '__main__':
    unittest.main()